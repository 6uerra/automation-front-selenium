# frozen_string_literal: true

After do
  clear_data_browser
end

Before('@home') do
  openbrowser('base')
end

Before('@result') do
  openbrowser('result')
  value_result
  $not_much_before = $not_much
  $the_sky_before = $the_sky
  $la_before = $la
  openbrowser('base')
end
