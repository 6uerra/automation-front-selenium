Feature: cases in which it is required to carry out a vote and validate its result with the different combinations
      
        @result
        Scenario Outline: Selection of the different answer options
            Given I enter in what up
              And I want to vote for the option that I like the most  "<option>"
             Then I validate the result of the vote

        Examples:
                  | option   |
                  | Not much |
                  | The sky  |
                  | La la la |

        @home
        Scenario Outline:  Do not select any option
            Given I enter in what up
              And not select any option
             Then valid for the error message "You didn't select a choice."
