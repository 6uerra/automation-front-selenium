# frozen_string_literal: true

require 'selenium-cucumber'
require 'selenium-webdriver'
require 'rspec'

And 'I want to vote for the option that I like the most  {string}' do |option|
  $value_option = option
  choice_polls($value_option)
  vote
end

And 'not select any option' do
  vote
end

And 'valid for the error message {string}' do |message|
  (validate_message).should eq(message)
end
