# frozen_string_literal: true

require 'selenium-cucumber'
require 'selenium-webdriver'
require 'rspec'

Then 'I validate the result of the vote' do
  value_result
  validate_result($value_option).should == true
  sleep 2
end
