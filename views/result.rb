# frozen_string_literal: true

module Result
  # For the elements I use the structure of {access_type}_{type}__{name_element}
  @@xpath_txt_choice1 = '//li[contains(.,"Not much --")]'
  @@xpath_txt_choice2 = '//li[contains(.,"The sky --")]'
  @@xpath_txt_choice3 = '//li[contains(.,"La la la --")]'

  # Get result values of polls
  def value_result
    $not_much = get_int(get_element_text(:xpath, @@xpath_txt_choice1))
    $the_sky  = get_int(get_element_text(:xpath, @@xpath_txt_choice2))
    $la       = get_int(get_element_text(:xpath, @@xpath_txt_choice3))
  end

  #  I use this method to make the comparison of the values ​​obtained initially and then be compared
  def validate_result(option_value)
    result = {
      'Not much' => ($not_much_before + 1) == $not_much,
      'The sky' => ($the_sky_before + 1) == $the_sky,
      'La la la' => ($la_before + 1) == $la
    }
    result[option_value]
  end
end
World(Result)
