# frozen_string_literal: true

module Home
  # For the elements I use the structure of {access_type}_{type}__{name_element}
  @@link_what_up = 'What'

  def what_up
    wait_displays(:partial_link_text, @@link_what_up, 1)
    wait_and_click_element(:partial_link_text, @@link_what_up)
  end
end
World(Home)
