# frozen_string_literal: true

module Polls
  # For the elements I use the structure of {access_type}_{type}__{name_element}
  @@link_what_up = 'What'
  @@xpath_btn_vote = '//input[contains(@value,"Vote")]'
  @@xpath_txt_mesagge = '*//p/strong'

  # Method used to select any option sent from the feature
  def choice_polls(option)
    @@xp_rb_option = '//label[contains(.,"' + option + '")]'
    wait_displays(:xpath, @@xp_rb_option, 1)
    wait_and_click_element(:xpath, @@xp_rb_option)
  end

  # Click send response
  def vote
    wait_displays(:xpath, @@xpath_btn_vote, 1)
    wait_and_click_element(:xpath, @@xpath_btn_vote)
  end

  # Get text message error
  def validate_message
    wait_displays(:xpath, @@xpath_txt_mesagge, 3)
    get_element_text(:xpath, @@xpath_txt_mesagge)
  end
end
World(Polls)
